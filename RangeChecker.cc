/**
 * Write a description of class RangeChecker here.
 *
 * @author (Rima Aidah)
 * @version (301230035)
 */
import java.util.Scanner;
public class RangeChecker{
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        System.out.print("Masukan suatu bilangan:");
        int bilangan = input.nextInt();
        
        if (bilangan >= 1)
        {
            if (bilangan <= 100)
            {
                System.out.println("Bilangan tersebut berada di antara 1 dan 100.");
            } else
            {
                System.out.println("Bilangan tersebut tidak berada di antara 1 dan 100.");
            }
        } else 
            System.out.println("Bilangan tersebut adalah negatif.");
        input.close();
        }
    }
    
